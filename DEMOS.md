# Demos

Antes de presentar como correr el robot en simulación y de manera experimental, se explicara brevemente la razón de cada uno de los paquetes en el proyecto.

## Paquetes explicados

- arlo_auto_docking: este incluye todo lo necesario con el sistema de auto recarga, se encarga de monitorear la batería y de llevar al robot a su estación.
- arlo_control_msgs: contiene los mensajes y nodos usados para poder controlar el robot y su movimiento.
- arlo_controller: es solo un paquete viejo que se creo para pruebas.
- arlobot_description: contiene el urdf correspondiente al robot y además ciertas configuraciones de su estructura.
- arlobot_uv_launcher: contiene ciertos nodos sobre la funcionalidad del robot y además los archivos `.launch` principales.
- camera_test: es un paquete usado simplemente para hacer tests de las cámaras de intel.
- cartographer_launcher: tiene las configuraciones de cartographer junto a archivos `.launch` que lo lanzan junto con move_base.
- darknet_ros: se usa principalmente para el procesamiento de imágenes.
- differential_drive: se usa para manejar la cinemática del robot diferencial.
- distance: evalúa la distancia de una persona frente al robot y la comunica por un topic.
- fiducials: sirve para poder detectar las marcas de aruco.
- pedsim_ros: en simulación es útil para generar personas.
- ros_maps_to_pedsim: sirve para poder pasar mapas a simulaciones con pedsim.
- rplidar_ros: es el paquete para poder publicar extraer y publicar los datos del lidar.
- sfm_diff_robot: mueve al robot según el modelo de fuerzas sociales.
- turtlebot_sim: un paquete viejo que se uso para hacer pruebas con el turtlebot3.

## Simulación

Antes de que el robot pueda funcionar de manera correcta y de forma autónoma, es necesario generar primero un mapa.

### Mapeo

En simulación para mapear, seguir los siguientes pasos.

1. Abrir una terminal y correr el comando:

   ```bash
   roslaunch cartographer_launcher arlobot_uv_sim_map.launch
   ```

2. Para mover el robot, usar el siguiente comando en una nueva terminal:

   ```bash
   rosrun turtlebot3_teleop turtlebot3_teleop_key
   ```

3. Mover el robot usando el keyboard teleop y una vez que se vea bien el mapa en RViz, correr los siguiente comando en una nueva terminal:

   ```bash
   rosservice call /finish_trajectory 0
   rosservice call /write_state "{filename: '${HOME}/amr/amr_ws/src/cartographer_launcher/maps/sim/amr_uv_$(date +%s)_map.bag.pbstream'}"
   ```

### Funcionamiento completo

Ya con el mapa generado, se puede correr el entorno por completo con todo el robot con los siguientes pasos:

1. Modificar el archivo `cartographer_launcher/launch/arlobot_uv_sim_nav.launch` y especificar el nombre del mapa generado.

2. Correr el siguiente comando en una terminal:

   ```bash
   roslaunch arlobot_uv_launcher complete_robot_sim.launch
   ```

## Experimental

En la experimentación se hace uso de una computadora master y la comunicación con la jetson nano usando husarnet.

### Mapeo

Para poder usar de manera experimental el robot, es necesario conectarse con la Jetson Nano.

Seguir los siguientes pasos:

1. Tener lista la red wifi covibot.
2. Encender el robot dandole a los interruptores en su zona trasera inferior.
3. Conectarse al robot por ssh.
4. Correr el siguiente comando en la computadora master:

   ```bash
   roslaunch cartographer_launcher arlobot_exp_map.launch
   ```

5. Correr el siguiente comando en la jetson nano:

   ```bash
   roslaunch  cartographer_launcher arlobot_exp_map_jetson.launch
   ```

6. Mover el robot usando desde la computadora master el turtlebot keyboard teleop.
7. Una vez que se tenga mapeado el espacio, guardar el mapa como en la simulación.

### Funcionamiento completo

Para llevar a cabo el funcionamiento completo, simplemente seguir los siguientes pasos:

1. Conectarse a la jetson nano por ssh.
2. Modificar el archivo `cartographer_launcher/launch/arlobot_uv_exp_nav.launch` para escoger el mapa correspondiente.
3. Correr el siguiente comando en la computadora master:

   ```bash
   roslaunch arlobot_uv_launcher complete_robot_exp.launch
   ```

4. Correr el siguiente comando en la jetson nano:

   ```bash
   roslaunch cartographer_launcher arlobot_exp_nav_jetson.launch
   ```

5. Y en otra terminal de la jetson

   ```bash
   roslaunch cartographer_launcher arlobot_jetson_serial.launch
   ```

Finalmente, para que reconozcan que todo esta funcionando bien, en la carpeta `cartographer_launcher/graph` se encuentran los gráficos de las transformadas y de los nodos conectados, para el experimental y simulado.
